package it.polimi.guinea.twitter2twilio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class Twitter2Twilio {


	public static final String ACCOUNT_SID = "";
	public static final String AUTH_TOKEN = "";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Twitter twitter = TwitterFactory.getSingleton();
		AccessToken accessToken = null;
		
		
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("config.properties"));
			System.out.println("Loading data from config file...");
			String aTT = props.getProperty("accessTokenToken");
			String aTS = props.getProperty("accessTokenSecret");
			long atUID = Long.parseLong(props.getProperty("accessTokenUserID"));
			accessToken = new AccessToken(aTT, aTS, atUID);
		} catch (FileNotFoundException e2) {
			
		} catch (IOException e2) {
			
		}
		
		twitter.setOAuthConsumer("", "");
		
		if (accessToken != null) {
			twitter.setOAuthAccessToken(accessToken);
			System.out.println("Loading access token from file...");
		}
		else {
		    RequestToken requestToken = null;
			
		    try {
				requestToken = twitter.getOAuthRequestToken();
			} catch (TwitterException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			}
			
		    
		    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		    while (null == accessToken) {
		      System.out.println("Open the following URL and grant access to your account:");
		      System.out.println(requestToken.getAuthorizationURL());
		      System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
		      String pin = null;
		
		      try{
		    	  	pin = br.readLine();
		         if(pin.length() > 0){
		           accessToken = twitter.getOAuthAccessToken(requestToken, pin);
		           props.setProperty("accessTokenToken", accessToken.getToken());
		           props.setProperty("accessTokenSecret", accessToken.getTokenSecret());
		           props.setProperty("accessTokenUserID", Long.toString(accessToken.getUserId()));
		           props.store(new FileOutputStream("config.properties"), null);
		         }
		      } catch (TwitterException te) {
		        if(401 == te.getStatusCode()){
		          System.out.println("Unable to get the access token.");
		        }else{
		          te.printStackTrace();
		        }
		      } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    }
		    
		    		twitter.setOAuthAccessToken(accessToken);
		}
		
	    List<Status> statuses = null;
		try {
			statuses = twitter.getHomeTimeline();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Searching for '" + args[0] + "'");
		
	    for (Status status : statuses) {
	    		String userName = status.getUser().getName();
	    		String text = status.getText();
	    		//System.out.println(userName + " wrote: " + text);
	        if (text.contains(args[0])) {
	        		System.out.println("******** found a tweet with " + args[0] + " *********");	
				
	        		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
				 
			    // Build a filter for the SmsList
			    Map<String, String> params = new HashMap<String, String>();
			    params.put("Body", userName + " wrote: '" + text + "'");
			    params.put("To", "+");
			    params.put("From", "+");
			 
			    SmsFactory messageFactory = client.getAccount().getSmsFactory();
			    Sms message = null;
				try {
					message = messageFactory.create(params);
				    System.out.println(message.getSid());
				} catch (TwilioRestException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
	        }
	    }

		
		
		
	}

}

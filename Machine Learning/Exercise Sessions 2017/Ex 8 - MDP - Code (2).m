clear
clc
close all

%% MDP
n_states = 3;

R_sa = [0.9*0 + 0.1*20; ...
       0.3*-2 + 0.7*-27; ...
       0.4*0 + 0.6*20; ...
       0.3*-5 + 0.7*-100; ...
       0.2*0+0.8*50]

P_sas = [0.9 0.1 0;
        0.3 0.7 0;
        0.4 0.6 0;
        0 0.3 0.7;
        0.2 0 0.8]

%% Find optimal policy (brute force)
gamma = 0.9;
policy = [];
policy{1} = [1 0 0 0 0; 0 0 1 0 0; 0 0 0 0 1];
policy{2} = [0 1 0 0 0; 0 0 1 0 0; 0 0 0 0 1];
policy{3} = [1 0 0 0 0; 0 0 0 1 0; 0 0 0 0 1];
policy{4} = [0 1 0 0 0; 0 0 0 1 0; 0 0 0 0 1];

for ii = 1:4
    V(:,ii) = inv(eye(n_states) - gamma * policy{ii} * P_sas) * policy{ii} * R_sa;
end
V

%% Policy iteration
gamma = 0.9;
admissible_actions = [1 1 0 0 0; 0 0 1 1 0; 0 0 0 0 1];
policy = [0 1 0 0 0; 0 0 0 1 0; 0 0 0 0 1];
V = zeros(n_states,1);
V_old = ones(n_states,1);

while any(V_old ~= V)
    V_old = V
    % Policy evaluation
    V = inv(eye(n_states) - gamma * policy * P_sas) * policy * R_sa
    greedy_rev = R_sa + gamma * P_sas * V;
    % Policy improvement
    Q = repmat(greedy_rev',n_states,1) .* admissible_actions;
    Q(Q == 0) = - inf;
    policy = repmat(max(Q,[],2),1,5) == Q;
end
policy

%% Value iteration
gamma = 0.9;
V = zeros(n_states,1);
V_old = ones(n_states,1);
tol = 0.001;

while any(abs(V - V_old) > tol)
    V_old = V;
    greedy_rev = R_sa + gamma * P_sas * V;
    Q = repmat(greedy_rev',n_states,1) .* admissible_actions;
    Q(Q == 0) = - inf;
    V = max(Q,[],2);
end
V
policy = repmat(max(Q,[],2),1,5) == Q
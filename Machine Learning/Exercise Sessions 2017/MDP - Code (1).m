clear
clc
close all

%% MDP model (policy do nothing at each state) -> MP (Value form)
n_states = 3;
R = [0.9*0 + 0.1*20; ...
     0.4*0 + 0.6*20; ...
     0.2*0+0.8*50];
gamma = 0.9;
P = [0.9 0.1 0   ;  ...
     0.4 0.6 0   ;  ...
     0.2 0   0.8 ];

%% Bellman expectation equation for V
eig(P)
eig(gamma*P)
eig(eye(n_states) - gamma * P)

V_eq = inv(eye(n_states) - gamma*P) * R

%% Policy evaluation
V_old = zeros(n_states,1);
tol = 0.0001;
V = R;
while any(abs(V_old -  V) > tol)
    V_old = V;
    V = R + gamma * P * V
end
[V V_eq]

%% MDP model (policy do nothing at each state) -> MP (Action-value form)
R_sa = [0.9*0 + 0.1*20; ...
       0.3*-2 + 0.7*-27; ...
       0.4*0 + 0.6*20; ...
       0.3*-5 + 0.7*-100; ...
       0.2*0+0.8*50]

P_sas = [0.9 0.1 0;
        0.3 0.7 0;
        0.4 0.6 0;
        0 0.3 0.7;
        0.2 0 0.8]

policy = [1 0 0 0 0; ...
          0 0 1 0 0; ...
          0 0 0 0 1]

%Value computation
policy * R_sa
policy * P_sas

%% Bellman expectation equation for Q
Q_eq = inv(eye(5) - gamma * P_sas * policy) * R_sa

%% Recursive solution of Bellman equation for Q
Q = R_sa;
Q_old = zeros(5,1);
tol = 0.001;
n_rep = 0;

gamma = 0.99;
while any(abs(Q - Q_old) > tol)
    Q_old  = Q;
    Q = R_sa + gamma * P_sas * policy * Q
    n_rep = n_rep + 1;
end

n_rep

%% Let us consider a new policy
% (special offer, membership, do nothing)
policy_far = [0 1 0 0 0;
          0 0 0 1 0;
          0 0 0 0 1]

policy_mio = [1 0 0 0 0;
          0 0 1 0 0;
          0 0 0 0 1]
gamma = 0.5;
V_far = inv(eye(n_states) - gamma * policy_far * P_sas) * policy_far * R_sa
V_mio = inv(eye(n_states) - gamma * policy_mio * P_sas) * policy_mio * R_sa

gamma = 0.9;
V_far = inv(eye(n_states) - gamma * policy_far * P_sas) * policy_far * R_sa
V_mio = inv(eye(n_states) - gamma * policy_mio * P_sas) * policy_mio * R_sa

gamma = 0.99;
V_far = inv(eye(n_states) - gamma * policy_far * P_sas) * policy_far * R_sa
V_mio = inv(eye(n_states) - gamma * policy_mio * P_sas) * policy_mio * R_sa
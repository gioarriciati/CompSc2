function hystogram_plus(data,n_bins,mu,sigma)

n_samples = length(data);
mini = min(data);
maxi = max(data);
step = (maxi - mini) / n_bins;

gca();
histogram(data,n_bins);
hold on;
x = mini:0.0001:maxi;
pdf = n_samples * step * normpdf(x, mu, sigma);
plot(x,pdf,'r');

/************computer-planning.mod************/
param n;		#number of month in the life span
set J:=1..n;	#set of months

param m;		#max rental time
set I:=1..m;	#set of possible rental time

param c{I};		#cost of rental for the i-th rental time
param q{J};		#number of computer needed in month j

var x{J} >= 0;		#number of computer active in that month
var y{J,I} >= 0;	#number of computer rented in month j for i rental time

minimize totalCost:
	sum{j in J, i in I}( c[i]*y[j,i] );

subject to demand{j in J}:
	x[j] >= q[j];

#Active computers in month j = 
#rented computers in that month + 
#rented computers in previous months for a rental time which is greater or equal to the distance between the 
#previous month and the actual month
subject to activeComputers{j in J}:
	x[j] = sum{i in I}( y[j,i] ) + 
	sum{j1 in J, i in I: j1 < j and j1 + i - 1 >= j}( y[j1,i] );
data;

param n:= 4;
param m:= 3;

param q:=
	1	9,
	2	5,
	3	7,
	4	9;

param c:=
	1	200,
	2	350,
	3	450;
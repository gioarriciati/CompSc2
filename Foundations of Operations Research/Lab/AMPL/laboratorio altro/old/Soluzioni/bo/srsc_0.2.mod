set VMS;

param t_0;
param t_n > t_0;
set T := t_0 .. t_n;

param C >= 0;
param QOS_MIN := 0.8;
param r_dem{i in VMS, t in T} >= 0;
param c_dem{t in T} := sum{i in VMS} r_dem[i, t];

var C_sys >= 1;
var r_al{VMS, T};
var c_al{T};
var u{T};

#subject to QoS:
#	min{i in VMS, t in T}(r_al[i, t] / r_dem[i, t]) >= QOS_MIN;
#	min{i in VMS}
#		(sum{t in T}(r_al[i, t] / r_dem[i, t]))/card(T) >= QOS_MIN;
	

		
subject to C_demand {t in T}:
	c_al[t] <= C_sys;
	
subject to r_allocation{i in VMS, t in T}:
	r_al[i, t] = 
	if c_dem[t] <= C_sys
	then r_dem[i, t]
	else (r_dem[i, t]/c_dem[t])*C_sys;
	
subject to c_allocation{t in T}:
	c_al[t] = sum{i in VMS} r_al[i, t];

subject to usage{t in T}:
	u[t] = c_al[t] / C_sys;
	
#maximize u_min: min{t in T}u[t];

minimize i_lost_min: sum{t in T}(C*(1-u[t])*C_sys) + sum{t in T}
											sum{i in VMS}
												(C*(r_dem[i,t] - r_al[i,t]));


data;
set VMS := vm1 vm2;
param t_0 := 1;
param t_n := 50;

param C:=10;

param r_dem(tr): 
		vm1	vm2	:=
	1	387	33
	2	326	33
	3	2551	33
	4	193	170
	5	2592	114
	6	195	173
	7	2564	158
	8	2542	173
	9	2572	173
	10	2551	174
	11	173	405
	12	174	204
	13	196	196
	14	2513	200
	15	314	193
	16	204	195
	17	301	196
	18	207	202
	19	387	387
	20	2547	326
	21	200	333
	22	33	386
	23	274	207
	24	333	245
	25	2515	274
	26	33	276
	27	405	301
	28	2542	327
	29	202	329
	30	2504	329
	31	276	387
	32	158	314
	33	386	2551
	34	329	2702
	35	2524	2592
	36	114	2504
	37	2546	2515
	38	2538	2513
	39	2521	2521
	40	2546	2538
	41	2702	2564
	42	170	2546
	43	33	2542
	44	173	2551
	45	327	2572
	46	2519	2599
	47	329	2547
	48	2599	2524
	49	245	2519
	50	173	2542
######
# SETS

param n;

set CHEESE;
set MILK;
set DAYS := 1..n;
set DAYS0 := 0..n;

########
# PARAMS

param production_capacity;             # Production capacity, in kg per day
param warehouse_capacity;              # Warehouse capacity, in kg
param milk_needed {CHEESE,MILK};       # Milk needed to make one kg of each cheese, in lt
param cheese_demand {CHEESE,DAYS};     # Daily cheese demand, in kg
param warehouse_cost {CHEESE};         # Cost for storing cheese, in euro per kg per day
param milk_availability {MILK,DAYS};   # Daily milk availability, in lt 

###########
# VARIABLES
var x {CHEESE, DAYS}, >=0;             # Quantity of cheese i produced each day t
var z {CHEESE, DAYS0}, >=0;            # Quantity of cheese i stored each day t

####################
# OBJECTIVE FUNCTION

# The objective is to minimize he total warehouse cost
minimize total_cost:
	sum {i in CHEESE, t in DAYS} z[i,t] * warehouse_cost[i];

#############
# CONSTRAINTS

# The wharehouse is empty at the first day
subject to START{i in CHEESE}:
	z[i,0] = 0;

# For each day and each milk, the daily milk availability cannot be exceeded
subject to NOTEXCEED_MILK_AVAILABILITY {t in DAYS, l in MILK}:
	sum {i in CHEESE} x[i,t] * milk_needed[i,l] <= milk_availability[l, t];

# For each day, the production capacity cannot be exceeded 
subject to NOTEXCEED_PRODUCTION_CAPACITY {t in DAYS}:
	sum {i in CHEESE} x[i,t] <= production_capacity;

# For each day, and each cheese, there must be a balance among wharehouse, production and selling
subject to WAREHOUSE_BALANCE {t in DAYS, i in CHEESE}:
	z[i,t] = x[i,t] + z[i,t-1] - cheese_demand[i,t];

# For each day, the daily cheese demand has to be satisfied
# Note that these constraints are redundant, due to previous constraints and nonnegativity of z vars
subject to SATISFY_CHEESE_DEMAND {t in DAYS, i in CHEESE}:
	x[i,t] + z[i,t-1] >= cheese_demand[i,t];

# For each day, the wharehouse capacity cannot be exceeded
subject to NOTEXCEED_WAREHOUSE_CAPACITY {t in DAYS}:
	sum {i in CHEESE} z[i,t] <= warehouse_capacity;

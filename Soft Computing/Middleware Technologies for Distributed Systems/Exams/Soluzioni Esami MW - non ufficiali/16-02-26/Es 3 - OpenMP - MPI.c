#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 10
#define N_TH 4

int main(int argc, char *argv[] ){
	double data[N];
	int i, my_num;
	int sum[N_TH];
	double total_sum=0, avg=0;
	
	#pragma omp parallel shared(data)
	{
		my_num = omp_get_thread_num();
		sum[my_num] = 0;
		for(i=my_num; i < N; i+=N_TH){
			sum[my_num] += data[i];
		}
		
		#pragma omp barrier
		
		if(omp_get_thread_num()==0){
			for(i=0; i< N_TH, i++){
				total_sum += sum[i];
			}
			avg = sum/N; 
		}
		
		#pragma omp barrier
		
		for(i=my_num; i < N; i+=N_TH){
			data[i] /= avg;
		}
		
	}
}

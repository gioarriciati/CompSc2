In this zip file you will find some `patch` files with the solutions to the
homework assignments concerning the modification of the ACSE compiler.

A patch is none other than a very simple text file, which contains the
differences between the version of ACSE which implements the solution of the
assignment, and the unmodified version.

In the patch, all lines that begin with `+` were added, while the lines that
begin with `-` were removed.

With these files you can patch your own copy of ACSE to experiment with the
modified compiler.

1. Download the zip archive containing the latest version of the ACSE compiler.
   This archive is found on WeBeeP.
2. Extract the archive.
3. Open a terminal, then change the current directory to the root directory of
   the archive -- where the readme of ACSE is also located.
4. Copy the patch in that same directory
5. Execute the following command in the terminal to apply the patch:

     patch -p1 < the_patch_file_name.patch
     
**Note:** during the exam, it is not required to write your solution in the same
format as a `patch` file.
     
To compile the compiler, run the following command from the root
directory of ACSE:

  make

At that point, you can compile files with ACSE and run the compiled assembly
file using the MACE simulator.

For example, if you have a file called 'program.src', to compile it through
ACSE you should execute the following commands:

  ./bin/acse program.src   # if successful it produces 'output.asm'
  ./bin/asm output.asm     # if successful it produces 'output.o'
  ./bin/mace output.o      # it executes the compiled program
  
To check that the generated intermediate representation reflects what you
expect, you can verify the content of the 'output_frontend.log' file.
